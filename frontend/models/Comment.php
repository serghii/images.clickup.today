<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $post_id
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 */
class Comment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }
    
     public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
               // 'attributes' => [
               //     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                //    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                //],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'post_id' => 'Post ID',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }
    
    /**
     * Get author of the post
     * @return User|null
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
    
    /**
     * Get author of the comment
     * @return User|null
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * Count number comments of the post 
     * @return number
     */
    public static function commentCount($id)
    {
        $count = Comment::find()->where(['post_id' => $id])->count();
        return $count;
    }
}
