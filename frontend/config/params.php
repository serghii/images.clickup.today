<?php
return [
    'adminEmail' => 'admin@example.com',
    
    'maxFileSize' => 1024 * 1024 * 2, //10 Megabites
    
    'storageUri' => '/uploads/', //http://images.com/uploads/f1/d7/739f9a9c99294.jpg

    //настройки м.б. вложенными
    'profilePicture' => [
        'maxWidth' => 400,
        'maxHeight' => 300,
    ],
    
    'postPicture' => [
        'maxWidth' => 1024,
        'maxHeight' => 768,
    ],
    
    'feedPostLimit' => 200,
];
