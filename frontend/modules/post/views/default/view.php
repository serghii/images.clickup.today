<?php
/* @var $this yii\web\View */
/* @var $post frontend\models\Post */

use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use frontend\models\Comment;
use frontend\models\Post;
?>
<div class="page-posts no-padding">                    
    <div class="row">                        
        <div class="page page-post col-sm-12 col-xs-12">
            <div class="blog-posts blog-posts-large">
                <div class="row">
                            <article class="post col-sm-12 col-xs-12">                                            
                                <div class="profile-title">
                                        <a href="<?php echo Url::to(['/user/profile/view', 'nickname' => ($post['user_id'])]); ?>" >
                                            <?php if ($user->getPictureById($post['user_id'])->picture): ?>
                                            <img src="<?php echo Yii::$app->storage->getFile($user->getPictureById($post['user_id'])->picture); ?>" id="profile-picture" class="author-image" alt="" />
                                            <?php else: ?>
                                            <img src="<?php echo $user->getPicture(); ?>" id="profile-picture" class="author-image" />
                                            <?php endif;?>
                                        </a>
                                        <div class="author-name">
                                            <?php if ($post->user): ?>
                                                <?php echo Html::encode($post->user->username); echo' post' . '<br>'?>
                                            <?php endif; ?>                                                                                         
                                        </div>                                             
                                    
                                </div>
                                <div class="post-type-image">
                                    <a href="<?php echo Url::to(['/post/default/view', 'id' => $post['id']]); ?>">
                                        <img src="<?php echo $post->getImage(); ?>" />
                                    </a>
                                </div>
                                <div class="post-description">
                                    <p><span><?php echo Html::encode($post->description); ?><span></p>
                                </div>
                                <div class="post-bottom">
                                    <div class="post-date">
                                        <span><?php
                                            echo' Published  ' ;
                                            echo Yii::$app->formatter->asDatetime($post['created_at']);
                                            echo'<br>'
                                               ?></span>    
                                    </div>
                                    <div class="post-comments">
                                        <br>
                                                <a href="<?php echo Url::to(['/post/default/view', 'id' => $post['id']]); ?>">
                                                        <?php echo (Comment::commentCount($post['id'])); ?> comments
                                                </a>
                                    </div>
                                        <div class="post-report">
                                            <?php if (!Post::isReported($currentUser,$post['id'])): ?>
                                                    <a href="#" class="btn btn-default button-complain" data-id="<?php echo $post['id']; ?>">
                                                    Report post <i class="fa fa-cog fa-spin fa-fw icon-preloader" style="display:none"></i>
                                                    </a>
                                                <?php else: ?>
                                                    <p>Post has been reported</p>
                                                <?php endif; ?>   
                                        </div>
                                    </div>
                            </article>
                    <div class="col-md-6">
                        <?php if ($currentUser && $currentUser->id !== ($post->user_id)): ?>
                            Likes: <span class="likes-count"><?php echo $post->countLikes(); ?></span>
                        <a href="#" class="btn btn-primary button-like <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "display-none" : ""; ?>" data-id="<?php echo $post->id ?>">
                            Like&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up"></span>
                        </a>
                        <a href="#" class="btn btn-primary button-unlike <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "" : "display-none"; ?>" data-id="<?php echo $post->id ?>">
                            Unlike&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-down"></span>
                        </a>
                        <?php endif; ?>
                    </div>
                     <div class="col-md-6">
                        <?php if ($currentUser && $currentUser->id == ($post->user_id)): ?>
                            <a href="<?php echo Url::to(['/post/default/delete-post', 'id' => $post['id'],]); ?>" class="btn btn-danger">Delete post</a>    
                        <?php endif; ?>
                    </div>                   
                </div>
            </div>
        </div>
    </div>
</div>

<h3><?php echo ($count); ?> comments:</h3>

<div class="page-posts no-padding">                    
    <div class="row">                        
        <div class="page page-post col-sm-12 col-xs-12">
            <div class="blog-posts blog-posts-large">
                <div class="row">
                    <?php if ($commentlist): ?>
                        <?php foreach ($commentlist as $comments): ?>
                            <!--  lastposts item -->
                            <article class="post col-sm-12 col-xs-12">                                            
                                <div class="profile-title">
                                        <a href="<?php echo Url::to(['/user/profile/view', 'nickname' => ($comments['user_id'])]); ?>">
                                            <?php if ($user->getPictureById($comments['user_id'])->picture): ?>
                                                <img src="<?php echo Yii::$app->storage->getFile($user->getPictureById($comments['user_id'])->picture); ?>" id="profile-picture" class="author-image" alt="" />
                                            <?php else: ?>
                                                <img src="<?php echo $user->getPicture(); ?>" id="profile-picture" class="author-image" alt="default photo" />
                                            <?php endif; ?>
                                        </a>
                                        <div class="author-name">                                                   
                                            <?php echo Html::encode($user->getUsername($comments['user_id'])->username);
                                            echo' commented' . '<br>'
                                            ?>                                                   
                                        </div>
                                            <?php if ($currentUser && $currentUser->equals($user)): ?>

                                                
    
                                                     <?=FileUpload::widget([
                                                     'model' => $modelPicture,
                                                     'attribute' => 'picture',
                                                     'url' => ['/user/profile/upload-picture',], // your url, this is just for demo purposes,
                                                     'options' => ['accept' => 'image/*'],
                                                     'clientEvents' => [
                                                     'fileuploaddone' => 'function(e, data) {
                                                          if (data.result.success) {
                                                               $("#profile-image-success").show();
                                                               $("#profile-image-fail").hide();
                                                               $("#profile-picture").attr("src", data.result.pictureUri);
                                                                   } else {
                                                                          $("#profile-image-fail").html(data.result.errors.picture).show();
                                                                          $("#profile-image-success").hide();
                                                                          }
                                                     }',
                                                      ],
                                                        ]); ?>
                                                      
                                                      <?php endif; ?>
                                    
                                </div>
                                <div class="post-type-image">
                                    <a href="<?php echo Url::to(['/comment/default/view', 'id' => $comments['id']]); ?>">
                                        
                                    </a>
                                </div>
                                <div class="post-description">
                                    <p><span><?php echo Html::encode($comments['description']); ?><span></p>
                                </div>
                                <div class="post-bottom">
                                    <div class="post-comments">
                                        № <span><?php echo Html::encode($comments['id']); ?><span>
                                    </div>
                                    <div class="post-date">
                                        <span>Created:<?php
                                            echo Yii::$app->formatter->asDatetime($comments['created_at']);
                                               ?></span>   
                                    </div>
                                    <div class="post-comments">
                                        <a href="#"></a>
                                    </div>
                                    <div class="post-date">
                                        <span>Updated:<?php
                                            echo Yii::$app->formatter->asDatetime($comments['updated_at']);
                                            echo'<br>'
                                               ?></span>    
                                    </div>
                                        <div class="post-report">
                                            <?php if ($currentUser && $currentUser->id == ($comments['user_id'])): ?>
                                            <a href="<?php echo Url::to(['/post/default/update', 'id' => $post['id'], 'comment_id'=> $comments['id']]); ?>" class="btn btn-default">Update comment</a>    
                                        </div>
                                        <div class="post-report">
                                            <a href="<?php echo Url::to(['/post/default/delete', 'id' => $post['id'], 'comment_id'=> $comments['id']]); ?>" class="btn btn-danger">Delete comment</a>    
                                            <?php endif; ?>
                                        </div>
                                    </div>
                            </article>
                   <!-- lastposts item -->            
                        <?php endforeach; ?>
                        <?php else: ?>
                            <div class="col-md-12">
                                  Nobody comment yet! Be first!
                            </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="comment-info">
    <div class="col-md-4">
    <h4>Create a comment</h4>

    <?php $form = ActiveForm::begin(); ?>    
        <?php //echo $form->field($comment, 'description'); ?>
        <?= $form->field($comment, 'description')->textarea(['rows' => '5']) ?>
        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-8">
                <?php echo Html::submitButton('Create comment', ['class' => 'btn btn-primary']); ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    </div>
    
</div>

<?php $this->registerJsFile('@web/js/likes.js', [
    'depends' => JqueryAsset::className(),
]);
$this->registerJsFile('@web/js/complaints.js', [
    'depends' => JqueryAsset::className(),
]);

