<?php
/* @var $this post\default\index */
/* @var $post frontend\models\Post */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\models\Comment;
use frontend\models\Post;
use yii\web\JqueryAsset;
$this->title = 'Last news';
?>
<div class="page-posts no-padding">                    
    <div class="row">                        
        <div class="page page-post col-sm-12 col-xs-12">
            <div class="blog-posts blog-posts-large">
                <div class="row">
                    <?php if ($lastposts): ?>
                        <?php foreach ($lastposts as $post): ?>
                            <?php /* @var $lastposts Post */ ?>
                            <!--  lastposts item -->
                            <article class="post col-sm-12 col-xs-12">                                            
                                <div class="profile-title">
                                        <a href="<?php echo Url::to(['/user/profile/view', 'nickname' => ($post['user_id'])]); ?>" >
                                            <?php if ($user->getPictureById($post['user_id'])->picture): ?>
                                            <img src="<?php echo Yii::$app->storage->getFile($user->getPictureById($post['user_id'])->picture); ?>" id="profile-picture" class="author-image" alt="" />
                                            <?php else: ?>
                                            <img src="<?php echo $user->getPicture(); ?>" id="profile-picture" class="author-image" />
                                            <?php endif;?>
                                        </a>
                                        <div class="author-name">                                                   
                                            <?php echo Html::encode($user->getUsername($post['user_id'])->username);
                                            echo' post' . '<br>'
                                            ?>                                                   
                                        </div>                                             
                                    
                                </div>
                                <div class="post-type-image">
                                    <a href="<?php echo Url::to(['/post/default/view', 'id' => $post['id']]); ?>">
                                        <img src="<?php echo Yii::$app->storage->getFile($post['filename']); ?>" />
                                    </a>
                                </div>
                                <div class="post-description">
                                    <p><span><?php echo Html::encode($post['description']); ?><span></p>
                                </div>
                                <div class="post-bottom">
                                    <div class="post-likes">
                                        <i class="fa fa-lg fa-heart-o"></i>
                                        <?php if ($currentUser && $currentUser->id !== ($post['user_id'])): ?>
                                        <span class="likes-count"><?php echo Post::likesCount($post['id']); ?></span>
                                            &nbsp;&nbsp;&nbsp;
                                        <a href="#" class="btn btn-default button-unlike <?php echo ($currentUser->likesPost($post['id'])) ? "" : "display-none"; ?>" data-id="<?php echo $post['id']; ?>">
                                            Unlike&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-down"></span>
                                        </a>
                                        <a href="#" class="btn btn-default button-like <?php echo ($currentUser->likesPost($post['id'])) ? "display-none" : ""; ?>" data-id="<?php echo $post['id']; ?>">
                                            Like&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up"></span>
                                        </a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="post-comments">
                                        <br>
                                                <a href="<?php echo Url::to(['/post/default/view', 'id' => $post['id']]); ?>">
                                                        <?php echo (Comment::commentCount($post['id'])); ?> comments
                                                </a>
                                    </div>
                                    <div class="post-date">
                                        <span><?php
                                            //echo ' Published  ' ;
                                            echo Yii::$app->formatter->asDatetime($post['created_at']);
                                            //echo'<br>'
                                               ?>
                                        </span>    
                                    </div>
                                        <div class="post-report">
                                            <?php if (!Post::isReported($currentUser,$post['id'])): ?>
                                                <a href="#" class="btn btn-default button-complain" data-id="<?php echo $post['id']; ?>">
                                                    Report post <i class="fa fa-cog fa-spin fa-fw icon-preloader" style="display:none"></i>
                                                </a>
                                            <?php else: ?>
                                                <p>Post has been reported</p>
                                            <?php endif; ?>     
                                        </div>
                                </div>
                            </article>
                   <!-- lastposts item -->           
                        <?php endforeach; ?>
                        <?php else: ?>
                            <div class="col-md-12">
                                  Nobody posted yet!
                            </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJsFile('@web/js/likes.js', [
    'depends' => JqueryAsset::className(),
]);
$this->registerJsFile('@web/js/complaints.js', [
    'depends' => JqueryAsset::className(),
]);


        
       