<?php

namespace frontend\modules\post\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use frontend\models\Post;
use frontend\models\Comment;
use frontend\modules\post\models\forms\PostForm;
use yii\helpers\ArrayHelper;
use frontend\models\User;
use frontend\modules\comment\models\forms\CommentForm;

/**
 * Default controller for the `post` module
 */
class DefaultController extends Controller
{
    /**
     * renders the create view for the moduls.
     *
     * @return string
     */
    public function actionCreate()
    {
       if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }
        $model = new PostForm(Yii::$app->user->identity);
        
        if ($model->load(Yii::$app->request->post())) {
            
            $model->picture = UploadedFile::getInstance($model, 'picture');
        
            if ($model->save()) {
                
                Yii::$app->session->setFlash('success', 'Post created!');
                return $this->goHome();
            }
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    /**
     * Renders the create view for the module
     * @return string
     */
    public function actionView($id)
    {
       /*@var $currentUser User*/
        $currentUser = Yii::$app->user->identity;
        $comment = new CommentForm(Yii::$app->user->identity);
        $post_id = $id;
        $user = new User;
        $count = Comment::commentCount($post_id);
        
        if ($comment->load(Yii::$app->request->post())){
            $comment->post_id = $post_id;
            if ($comment->save()) {
                Yii::$app->session->setFlash('success', 'Comment created!');
                return $this->refresh(); 
                } else {
                Yii::$app->session->setFlash('info','Sorry, an unexpected error has occurred!');
            }
        }      
        return $this->render('view', [
            'post' => $this->findPost($id),
            'currentUser' => $currentUser,
            'comment' => $comment,
            'post_id ' => $post_id,
            'commentlist' => $this->findCommentsForPost($id),
            'user' => $user,
            'count' => $count,
        ]);   
    }
    
       /**
     * Renders the updateate view for the module
     * @return string
     */
    public function actionUpdate($id, $comment_id)
    {
       /*@var $currentUser User*/
        $currentUser = Yii::$app->user->identity;
        $commentup = new CommentForm(Yii::$app->user->identity);
        $comment = Comment::findOne($comment_id);
        $post_id = $id;
        $user = new User;
        $count = Comment::commentCount($post_id);
        if ($commentup->load(Yii::$app->request->post())){
            $comment->description = $commentup->description;;
            if ($comment->update()) {
                Yii::$app->session->setFlash('success', 'Comment updated!');
                return $this->redirect(['/post/default/view', 'id' => $id ]);
                } else {
                Yii::$app->session->setFlash('info','Sorry, an unexpected error has occurred!');
            }
        }
        
        return $this->render('update', [
            'post' => $this->findPost($id),
            'currentUser' => $currentUser,
            'commentup' => $commentup,
            'post_id ' => $post_id,
            'commentlist' => $this->findCommentsForPost($id),
            'user' => $user,
            'comment_id' =>  $comment_id,
            'count' => $count,
            ]);   
    }
    /**
     * @param integer $id is post_id
     * @param integer $comment_id is id of the comment
     * @return view of the post with comments
     */
     public function actionDelete($id, $comment_id)
    {
         print"<script>alert('Delete this comment?')</script>";
         $model = Comment::findOne($comment_id);
         $model->delete();
         Yii::$app->session->setFlash('success', 'Comment has been deleted!');
         return $this->redirect(['/post/default/view', 'id' => $id]);
    }
    /**
     * @param integer $id is the post id 
     * @return view of the posts
     * delete post and linked feeds,comments,likes,complains
     */
     public function actionDeletePost($id)
    {
         print"<script>alert('Delete this post?')</script>";
         $db = Yii::$app->db;
         $post_id = $id;
        $transaction = $db->beginTransaction();
        try {
            $db->createCommand()->delete('post', 'id = :id', [':id' => $id])->execute();
            $db->createCommand()->delete('feed', 'post_id = :post_id', [':post_id' => $post_id])->execute();
            $db->createCommand()->delete('comment', 'post_id = :post_id', [':post_id' => $post_id])->execute();
            $delLikes = Post::deleteLikes($id);
            $delComplaints = Post::deleteComplains($id);
             $transaction->commit();
           
         Yii::$app->session->setFlash('success', 'Post has been deleted!');
         return $this->redirect(['/post/default/index']);
        
        } catch(\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }
     /**
     * @param integer $id
     * @return User
     * @throws NotFoundHttpException
     */
    private function findPost($id)
    {
        if ($user = Post::findOne($id)) {
            return $user;
        }
        throw new NotFoundHttpException();
    }
    
     public function actionLike()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        $post = $this->findPost($id);
        
        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;        
        
        $post->like($currentUser);

        return [
            'success' => true,
            'likesCount' => $post->countLikes(),
        ];     
    }
    
    public function actionUnlike()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;
        $post = $this->findPost($id);

        $post->unLike($currentUser);

        return [
            'success' => true,
            'likesCount' => $post->countLikes(),
        ];
    }
    
    public function actionComplain()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;
        $post = $this->findPost($id);

        if ($post->complain($currentUser)) {
            return [
                'success' => true,
                'text' => 'Post reported'
            ];
        }
        return [
            'success' => false,
            'text' => 'Error',
        ];
    }
    public function actionIndex()
    {
        $user = new User;
        $currentUser = Yii::$app->user->identity;
        
        return $this->render('index', [
            'lastposts' => $this->findLastPosts(),
            'user' => $user,
            'currentUser' => $currentUser,
        ]); 
          
    }
    /**
     * @param integer $id
     * @return User
     * @throws NotFoundHttpException
     */
    private function findLastPosts()
    {   
        $order = ['created_at' => SORT_DESC];
        $limit = Yii::$app->params['maxPostsOnLastNewsPage'];
        if ($user = Post::find()->asArray()->indexBy('id')->orderBy($order)->limit($limit)->all()) {
            return $user;
        }
        throw new NotFoundHttpException();
    }
    
    
    
    /**
     * @param integer $post_id
     * @return User
     * @throws NotFoundHttpException
     */
    private function findCommentsForPost($post_id)
    {   
        $order = ['created_at' => SORT_ASC];
        
        if ($user = Comment::find()->asArray()->where('post_id = :post_id', [':post_id' => $post_id])->indexBy('id')->orderBy($order)->all()) {
            return $user;
        }
        //throw new NotFoundHttpException();
    }
    
}
