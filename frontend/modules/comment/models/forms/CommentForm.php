<?php

namespace frontend\modules\comment\models\forms;

use Yii;
use yii\base\Model;
use frontend\models\Comment;
use frontend\models\User;

class CommentForm extends Model
{
   const MAX_DESCRIPTION_LENGHT = 1000;
   
   public $description;
   public $post_id;
   
   private $user;
      
   public function rules()
    {
        return [
            
            [['description'], 'string', 'max' => self::MAX_DESCRIPTION_LENGHT],
            //[['post_id'], 'string', 'max' => 11],
        ];
    }
    
     public function __construct(User $user)
    {
       $this->user = $user;
    }
    
    /**
     * @return boolean
     */
    public function save()
    {
        if ($this->validate()){
            $comment = new Comment();
            $comment->description = $this->description;
            //$comment->created_at = time();
            //$comment->updated_at = time();
            $comment->user_id = Yii::$app->user->identity->id;
            $comment->post_id = $this->post_id;
            
            
            //echo '<pre>';
            //print_r($comment);
            //echo '<pre>';//die;
            return $comment->save(false);
        }
        //return false;
    }
}
