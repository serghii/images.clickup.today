<?php
/* @var $this yii\web\Index */
/* @var $post frontend\models\Comment */

use yii\helpers\Html;


?>
<div class="post-default-index">
    
    <h1>View comments</h1>
    <div class="row">
        
        <div class="col-md-12">
            <?php if ($lastcomments): ?>
                <?php foreach ($lastcomments as $comment): ?>
        </div>
        
        <div class="author-name">                                                   
            <?php echo Html::encode($user->getUsername($comment['user_id'])->username);
                echo' commented' . '<br>'
            ?>                                                   
        </div>       
        <div class="post-description">
            <p><span><?php echo Html::encode($comment['description']); ?><span></p>
        </div>
            <?php endforeach; ?>
    </div>
          <?php endif; ?>
   
