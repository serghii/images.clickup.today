<?php
/* @var $this yii\web\View */
/* @var $post frontend\models\Comment */

use yii\helpers\Html;
use yii\web\JqueryAsset;

?>
<div class="post-default-index">
    
    <h1>View comment</h1>
    <div class="row">
        
        <div class="col-md-12">
            <?php if ($model->user): ?>
                <?php echo $model->user->username; ?>
            <?php endif; ?>
        </div>
        
        <div class="col-md-12">
           <span><?php echo Html::encode($model->created_at); ?><span>
        </div>       
        <div class="col-md-12">
            &nbsp;&nbsp;<span><?php echo Html::encode($model->description); ?><span>
        </div>

    </div>
    
   
