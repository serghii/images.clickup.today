<?php
/* @var $this yii\web\View */
/* @var $model frontend\modules\post\models\forms\CommentForm */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<!-- <div class="post-default-index"> -->
<div class="post-comments">
    
    <h1>Create comment</h1>

    <?php $form = ActiveForm::begin(); ?>
    
    
        <?php echo $form->field($model, 'description'); ?>
        <?php echo $form->field($model, 'post_id'); ?>
    
        <?php echo Html::submitButton('Comment'); ?>
    
    <?php ActiveForm::end(); ?>
    
</div>
