<?php

namespace frontend\modules\comment\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\comment\models\forms\CommentForm;
use frontend\models\User;
use frontend\models\Comment;

/**
 * Default controller for the `comment` module
 */
class DefaultController extends Controller
{
    public function actionCreate()
    {       
        $model = new CommentForm;
        
        if ($model->load(Yii::$app->request->post())) {
                    
            if ($model->save()) {
                
                Yii::$app->session->setFlash('success', 'Comment created!');
                return $this->goHome();
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    
    /**
     * @return all last comments array
    */
    public function actionIndex()
    {
        $user = new User;
        $model = new CommentForm;
        
        if ($model->load(Yii::$app->request->post())) {
                    
            if ($model->save()) {
                
                Yii::$app->session->setFlash('success', 'Comment created!');
                return $this->goHome();
            }
        }
        return $this->render('index', [
            'lastcomments' => $this->findLastComments(),
            'user' => $user,
            'model' => $model,
        ]); 
          
    }
    /**
     * @param integer $id
     * @return User
     * @throws NotFoundHttpException
     */
    private function findLastComments()
    {   
        $order = ['created_at' => SORT_DESC];
        $limit = Yii::$app->params['maxPostsOnLastNewsPage'];
        if ($user = Comment::find()->asArray()->indexBy('id')->orderBy($order)->limit($limit)->all()) {
            return $user;
        }
        throw new NotFoundHttpException();
    }
    
}
